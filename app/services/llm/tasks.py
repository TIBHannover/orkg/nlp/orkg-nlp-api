# -*- coding: utf-8 -*-
from typing import Any, Dict, List, Union

JSON = Union[Dict[str, Any], List[Any]]


class Task:
    """
    Base class for all tasks.
    """

    # The system prompt that controls the behaviour of the model.
    system_prompt: str

    # The user prompt that is formatted with the named placeholders unless you override the format_user_prompt function.
    user_prompt: str

    # The functions that are used to extract the arguments from the model output, can be set to [] if not used.
    functions: JSON

    def __init__(self, system_prompt: str, user_prompt: str, functions: JSON):
        self.system_prompt = system_prompt
        self.user_prompt = user_prompt
        self.functions = functions

    def format_user_prompt(self, **kwargs) -> str:
        return self.user_prompt.format(**kwargs)

    def arguments_post_processor(self, output: Any, **kwargs) -> Any:
        return output


########################
# Recommendation Tasks #
########################


class RecommendAnnotationTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt=(
                "You are Scientific Named Entity Recognition (NER) Assistant: \n"
                "You are a specialized Assistant required to perform the NER task with the purpose of identifying key "
                "entities to research contribution to instantiate the open research knowledge graph. "
                "focused on identifying entities specifically in scientific text.\n\n"
                "Now, extract the following scientific named entities from the given "
                "scientific research paper title or abstract.\n"
                "1. Research Problem: A research problem represents the specific challenge or issue that a "
                "contribution in a research paper aims to address. It is typically expressed as a concise "
                "phrase summarizing the core problem tackled by the study. It is usually a maximum of 4 words.\n"
                "2. Material: The material refers to the key resources or entities utilized in a research contribution"
                " to address the stated research problem.\n"
                "3. Method: The method represents ab approach, technique, or framework applied in a research "
                "contribution to address the specified research problem using the provided material.\n"
                "4. Result: The result represents the key findings or outcomes derived from a research contribution "
                "after applying a specific method to a given material in addressing the research problem.\n\n"
                "Also consider the following: \n"
                "If no entity is found per type, return an empty array.\n"
                "Only output entities that are explicitly present in the input text, using **exact phrases**.\n"
                "**Do not infer** or include entities that are not mentioned.\n"
                "Do not introduce new entities. Only consider the four entities that we described.\n"
            ),
            user_prompt="{text}",
            functions=[
                {
                    "name": "extract_scholarly_entities",
                    "description": "Extract scholarly from a given text.",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "research_problem": {
                                "type": "array",
                                "description": "The research problem investigated in the text.",
                                "items": {"type": "string"},
                            },
                            "material": {
                                "type": "array",
                                "description": "The key resources utilized in a research contribution.",
                                "items": {"type": "string"},
                            },
                            "method": {
                                "type": "array",
                                "description": "The method represents the approach, technique, or framework.",
                                "items": {"type": "string"},
                            },
                            "result": {
                                "type": "array",
                                "description": "The result  represents the key findings from a research contribution.",
                                "items": {"type": "string"},
                            },
                        },
                        "required": ["research_problem", "material", "method", "result"],
                    },
                },
            ],
        )

    def arguments_post_processor(self, output: Any, **kwargs) -> Any:
        inputs = kwargs["text"]
        annotation = []
        annotation_id = 1
        inputs = inputs.lower()
        for ner_type, entities in output.items():
            for entity in entities:
                positions = []
                entity = entity.lower()
                start_index = 0
                while start_index < len(inputs):
                    start_index = inputs.find(entity, start_index)
                    if start_index == -1:
                        break
                    end_index = start_index + len(entity)
                    positions.append((start_index, end_index - 1))
                    start_index = end_index
                if positions:
                    for i, (start, end) in enumerate(positions, start=1):
                        annotation.append(
                            {
                                "id": f"T{annotation_id}",
                                "extraction": entity,
                                "class": ner_type.upper(),
                                "span": [start, end],
                                "confidence": 1,
                            }
                        )
                        annotation_id += 1
        return {"values": annotation}


class RecommendPropertiesTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="You are an assistant for building a knowledge graph for science. "
            "Your task is to recommend additional related predicates based on "
            "the set of existing predicates. Recommend a list maximum 5 additional predicates.",
            user_prompt="The existing predicates are: {0}",
            functions=[
                {
                    "name": "getProperties",
                    "description": "The array of additionally recommended properties",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "properties": {
                                "type": "array",
                                "description": "The recommended properties",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["properties"],
                    },
                },
            ],
        )

    def format_user_prompt(self, **kwargs) -> str:
        """
        This is a special case where the user prompt is formatted with a list of properties.
        Hence, we need to override the default behaviour of the base class formatting function.
        """
        return self.user_prompt.format(", ".join(kwargs["properties"]))


class RecommendPropertiesDescriptionTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="You are an assistant for building a FAIR knowledge graph for science. "
            "Your task is to provide a description for a given research predicate. "
            "Research predicates are selected salient properties that help describe in a structured manner "
            "the main contribution of a research paper.",
            user_prompt="Describe the following predicate: {predicate}, identified as a salient property to describe "
            'the research contribution of the paper titled: "{title}", in the research field: {field}',
            functions=[
                {
                    "name": "getPropertyDescription",
                    "description": "Get property description",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "description": {
                                "type": "string",
                                "description": "The recommended description of the property",
                            },
                        },
                        "required": ["description"],
                    },
                },
            ],
        )


class RecommendResearchProblemsTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="A research problem contains a maximum of approximately 4 words "
            "to explain the research task or topic of a paper. Provide a list of maximum 5 research problems "
            "based on the title and optionally abstract provided by the user.",
            user_prompt="{paperTitle} {abstract}",
            functions=[
                {
                    "name": "getResearchProblems",
                    "description": "Get a array of research problems",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "values": {
                                "type": "array",
                                "description": "The research problems",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["values"],
                    },
                },
            ],
        )


class RecommendMethodsTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="Extract a list of maximum 5 methods from a scientific paper "
            "based on the title and optionally abstract provided by the user. "
            "if no methods are found, return an empty array.",
            user_prompt="{paperTitle} {abstract}",
            functions=[
                {
                    "name": "getMethods",
                    "description": "Get a array of methods",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "values": {
                                "type": "array",
                                "description": "The methods",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["values"],
                    },
                },
            ],
        )


class RecommendMaterialsTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="Extract a list of maximum 5 materials that are used in a scientific paper."
            "Extract it from the title and optionally abstract provided by the user. "
            "if no materials are found, return an empty array.",
            user_prompt="{paperTitle} {abstract}",
            functions=[
                {
                    "name": "getMaterials",
                    "description": "Get a array of materials",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "values": {
                                "type": "array",
                                "description": "The materials",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["values"],
                    },
                },
            ],
        )


class RecommendPropertiesFromTextTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="Act as an ORKG researcher. Return in JSON. Provide only property names "
            "without values or extra information. Recommend a maximum of 3 to 4 properties for each text "
            "selection.",
            user_prompt="Find the best property names for the selected text: {selectedText}",
            functions=[
                {
                    "name": "getProperties",
                    "description": "The array of additionally recommended properties",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "properties": {
                                "type": "array",
                                "description": "The recommended properties",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["properties"],
                    },
                },
            ],
        )


########################
#  Verification Tasks  #
########################


class CheckDescriptivenessTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="Provide feedback to a user on how to improve a provided description text. The "
            "description text should give information about the objectives and topics of a scientific "
            "tabular related work overview. ",
            user_prompt="{value}",
            functions=[
                {
                    "name": "getFeedback",
                    "description": "Get feedback for the provided description",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "feedback": {
                                "type": "string",
                                "description": "The feedback for the provided description",
                            },
                        },
                        "required": ["feedback"],
                    },
                },
            ],
        )


class CheckResourceDestructuringTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="You are an assistant for building a knowledge graph for science. Provide advice on if "
            "and how to decompose a provided resource label into separate resources. Only provide feedback is "
            "decomposing makes sense. Return the feedback in JSON.",
            user_prompt="{label}",
            functions=[
                {
                    "name": "provideDecomposeFeedback",
                    "description": "Provide feedback on how a resource label can be decomposed",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "feedback": {
                                "type": "string",
                                "description": "A brief one paragraph explanation explaining how "
                                "the resource can be decomposed",
                            },
                        },
                        "required": ["feedback"],
                    },
                },
            ],
        )


class CheckIfLiteralTypeIsCorrectTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="You are an assistant in building a knowledge graph for science. You task is to advice "
            "users whether they should use a RDF resource or RDF literal. Based on a user-provided label, advice "
            "whether the type should be 'literal' or 'resource'. Literals are generally larger pieces of text and "
            "are not reusable, resource are atomic and can be reused. Return in JSON.",
            user_prompt="{label}",
            functions=[
                {
                    "name": "getFeedback",
                    "description": "Provide feedback on whether the label should be a 'literal' or 'resource'",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "feedback": {
                                "type": "string",
                                "description": "Brief one paragraph explanation why the type should be literal or "
                                "resource",
                            },
                        },
                        "required": ["feedback"],
                    },
                },
            ],
        )


class CheckPropertyLabelGuidelinesTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="You are an assistant in building a knowledge graph for science. Provide feedback "
            "whether the provided predicate label is generic enough to make it reusable in the graph and explain "
            "how to make it more generic. Examples of properties that are not reusable: population in Berlin "
            "(because it contains a location), temperature in degrees Celsius (because it contains a unit). "
            "Return in JSON.",
            user_prompt="The label is: {label}",
            functions=[
                {
                    "name": "getFeedback",
                    "description": "Determine whether the label is generic enough to make it reusable in the graph",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "feedback": {
                                "type": "string",
                                "description": "Feedback whether the property is reusable or not",
                            }
                        },
                        "required": ["feedback"],
                    },
                },
            ],
        )


class GenerateResearchQuestionsTask(Task):
    def __init__(self):
        super().__init__(
            system_prompt="Generate five research questions based on the provided keywords. "
            "The research questions should be related to the keywords and should be open-ended. "
            "The questions should be interesting and should not be too broad or too narrow.",
            user_prompt="Keywords: {keywords}",
            functions=[
                {
                    "name": "getResearchQuestions",
                    "description": "Get a list of research questions",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "questions": {
                                "type": "array",
                                "description": "The research questions",
                                "items": {
                                    "type": "string",
                                },
                            },
                        },
                        "required": ["questions"],
                    },
                },
            ],
        )
